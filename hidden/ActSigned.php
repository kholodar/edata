<div class="mfiModal actPopupWrap zoomAnim">



   <div class="actPopupS">

		<div class="actTitleS">Акт Виконаних Робіт  до Договору № 123 44213 від 12.09.2015</div>



		<div class="wrapperAct">

			<div class="itemsAct">

				<span class="titleLeft">Контрагент</span>

				<span class="descrLeftItem dopColorF">Пат “Зіронька” код п/п 123123123</span>

			</div>

			<div class="itemsAct">

				<span class="titleLeft">Місцезнаходження</span>

				<span class="descrLeftItem dopColorF">Україна, Київська обл., м. Біла Церква, Центральний район, вул. Старовокзальна 23а, корпус 1, квартира/офіс 245</span>

			</div>

			<div class="otherWrapperAct">
				<div class="itemsAct">

					<span class="titleLeft">Акт</span>

					<span class="descrLeftItem dopColorF">1232123 від 12.09.1025</span>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Керівник</span>

					<span class="descrLeftItem dopColorF">Іванов Іван Іванович</span>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Вартість послуг</span>

					<span class="descrLeftItem dopColorF">12 345 .00 грн</span>

				</div>

				<div class="itemsAct">

					<div class="titleLeft">

						<span>Ціна за одиницю</span>

						<span>(за наявності)</span>

					</div>

					<div class="descrLeftItem dopColorF">

						<span>23 .25 грн</span>

						<span>Кількість закупленного товару, робіт та/або послуг: 23412 шт</span>

					</div>

				</div>

			</div>

		</div>



   </div>



</div>