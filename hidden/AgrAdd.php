<div class="mfiModal actPopupWrap zoomAnim">

   <div class="actPopupS">
		<div class="actTitleS">Акт Виконаних Робіт  до Договору № 123 44213 від 12.09.2015</div>

		<div class="wrapperAct">
			<div class="itemsAct">
				<span class="titleLeft">Контрагент</span>
				<span class="descrLeftItem">Пат “Зіронька” код п/п 123123123</span>
			</div>
			<div class="itemsAct">
				<span class="titleLeft">Місцезнаходження</span>
				<span class="descrLeftItem">Україна, Київська обл., м. Біла Церква, Центральний район, вул. Старовокзальна 23а, корпус 1, квартира/офіс 245</span>
			</div>
			<div class="otherWrapperActSigned">
				<div class="wForm wFormFilter wFormDef" data-form="true">
		            <div class="wFormRow row_top_line">
						<label for="edrp" class="signLabel">Акт №</label>
		                <input type="tel" id="edrpPl" name="sender" placeholder="номер акту" class="wInput" required data-rule-number="true" data-rule-minlength="8">
		            </div>
		            	<label for="edrp" class="signLabel">від</label>
		            	<div class="wFormRow row_top_line">
			                <input type="text" name="date_from" placeholder="" class="date_input" required >
			            </div>
						<label for="" class="signLabel">Керівник</label>
						<div class="wFormRow">
							<input type="text" id="edrpTa" name="recipientrer" placeholder="Прізвище" class="wInput" required data-rule-word="true" data-rule-minlength="2">
						</div>
						<div class="wFormRow">
							<input type="text" id="edrpTaqwe" name="recipientwe" placeholder="Ім'я" class="wInput" required data-rule-word="true" data-rule-minlength="2">
						</div>
						<div class="wFormRow">
							<input type="text" id="edrpTaqwer" name="recipientqw" placeholder="По-батькові" class="wInput" required data-rule-word="true">
						</div>
						<label for="edrpTa" class="signLabel">Вартість послуг</label>
			            <div class="wFormRow row_top_line">
			                <input type="tel" id="edrpTatr" name="recipientgf" placeholder="сумма" class="wInput" required data-rule-number="true">
			                <span>грн</span>
			            </div>
			            <label for="edrpTa" class="signLabel">Обсяг платежів за договором у звітному періоді</label>
						<div class="wFormRow row_top_line">
							<input type="tel" id="edrpTaqwere" name="recipientfgh" placeholder="сумма" class="wInput" required data-rule-number="true">
							<span>грн</span>
						</div>
		            <div class="wFormRow row_top_line">
						<label for="edrpTa" class="signLabel">
							<span>Ціна за одиницю</span>
							<span>(за наявності)</span>
						</label>
						<div class="wFormRow row_top_line">
							<input type="tel" id="edrpTa" name="recipient" placeholder="ціна" class="wInput" required data-rule-number="true">
							<span>грн</span>
						</div>
						<label for="edrpTa" class="signLabel">Кількість закупленного товару, робіт та/або послуг</label>
						<div class="wFormRow row_top_line">
							<input type="tel" id="edrpTa" name="recipient" placeholder="кількість" class="wInput" required data-rule-number="true">
						</div>
		                
		            </div>
		            <div class="wFormRow row_submit flr">
		            	<button type="submit" class="wSubmit button_edit"><span>Зберегти зміни</span></button>
		                <button type="submit" class="wSubmit button"><span>Створити кабінет</span></button>
		            </div>
		            <div class="clear"></div>
				</div>
			</div>
		</div>

   </div>

</div>