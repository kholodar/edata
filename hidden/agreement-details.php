﻿<div class="mfiModal zoomAnim">
   <div class="agreement_details_popup">
   		<div class="agreement_details_title">Договір № 123-2315523 від 12.09.2015</div>
   		<div class="agreement_details_subtitle">Додано та підписано 12.10.2015</div>

   		<div class="agreement_details_total">
   			<div class="agreement_details_total_item"><span>Підсумкова Вартість</span> 349 674 000.00 грн</div>
   			<div class="agreement_details_total_item"><span>Актів на Сумму</span> 235 215 750.00 грн</div>
   		</div>

   		<div class="agreement_details_options">
   			<dl>
   				<dt>Контрагент</dt>
   				<dd>Пат “Зіронька” код п/п 123123123</dd>
   			</dl>

   			<dl>
   				<dt>Місцезнаходження</dt>
   				<dd>Україна, Київська обл., м. Біла Церква, Центральний район, вул. Старовокзальна 23а, корпус 1, квартира/офіс 245</dd>
   			</dl>

   			<dl>
   				<dt>Керівник</dt>
   				<dd>Іваненко Іван Іванович</dd>
   			</dl>

   			<dl>
   				<dt>Строк дії договору</dt>
   				<dd>з 12.09.2015 по 12.09.2017, строк у місяцях — 12 місяців</dd>
   			</dl>

   			<dl>
   				<dt>Вартість договору</dt>
   				<dd>140 000 .00 грн. Обсяг платежів за договором у звітному періоді: 140 000 .00</dd>
   			</dl>

   			<dl>
   				<dt>Процедура закупівлі</dt>
   				<dd>Так</dd>
   			</dl>

   			<dl>
   				<dt>Стан виконання</dt>
   				<dd>Дiючий</dd>
   			</dl>
   		</div>

   		<div class="agreement_details_title">Додатки</div>

   		<table class="agreement_details_table">
   			<tr>
   				<th>Номер</th>
   				<th>Від</th>
   				<th>Предмет додаткової угоди</th>
   				<th>Строк дії з</th>
   				<th>по</th>
   				<th>у місяцях</th>
   				<th>Вартість</th>
   			</tr>

			<tr>
				<td>№ 12-12364/45</td>
				<td>12.09.2015</td>
				<td>Розширення строків дії Договору</td>
				<td>12.09.2015</td>
				<td>12.09.2016</td>
				<td>9</td>
				<td>123 000 .00</td>
			</tr>
			
			<tr>
				<td>№ 12-12364/45</td>
				<td>12.09.2015</td>
				<td>Зміна вартості</td>
				<td>12.09.2015</td>
				<td>12.09.2016</td>
				<td>9</td>
				<td>123 000 .00</td>
			</tr>
			
			<tr>
				<td>№ 12-12364/45</td>
				<td>12.09.2015</td>
				<td>Розширення строків дії Договору</td>
				<td>12.09.2015</td>
				<td>12.09.2016</td>
				<td>9</td>
				<td>123 000 .00</td>
			</tr>

			<tr>
				<td>№ 12-12364/45</td>
				<td>12.09.2015</td>
				<td>Зміна вартості</td>
				<td>12.09.2015</td>
				<td>12.09.2016</td>
				<td>9</td>
				<td>123 000 .00</td>
			</tr>
   		</table>

   		<div class="agreement_details_title">Акти Виконаних Робіт</div>

   		<table class="agreement_details_table">
   			<tr>
   				<th>Номер</th>
   				<th>Від</th>
   				<th>Предмет Акту виконаних робіт</th>
   				<th>Вартість</th>
   			</tr>

			<tr>
				<td>№ 12-12364/45</td>
				<td>12.09.2015</td>
				<td>Постачання тортів на шкільні свята</td>
				<td>123 000 .00</td>
			</tr>

			<tr>
				<td>№ 12-12364/45</td>
				<td>12.09.2015</td>
				<td>Постачання тортів за Жовтень</td>
				<td>123 000 .00</td>
			</tr>

			<tr>
				<td>№ 12-12364/45</td>
				<td>12.09.2015</td>
				<td>Постачання тістечок</td>
				<td>123 000 .00</td>
			</tr>

			<tr>
				<td>№ 12-12364/45</td>
				<td>12.09.2015</td>
				<td>Постачання тістечок</td>
				<td>123 000 .00</td>
			</tr>
   		</table>

   </div>
</div>