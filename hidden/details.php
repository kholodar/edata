﻿<div class="mfiModal zoomAnim">
   <div class="detail_popup">
   		<div class="detail_title">Деталі Транзакції</div>

   		<div class="detail_row clearFix">
   			<div class="detail_col3">
				<dl class="detail_dl">
					<dt>№ транзакції:</dt>
					<dd>12345 23 11 22345</dd>
				</dl>
				<dl class="detail_dl">
					<dt>код транзакції:</dt>
					<dd>ав 123 43 4421</dd>
				</dl>
   			</div>

   			<div class="detail_col3">
				<dl class="detail_dl">
					<dt>складання:</dt>
					<dd>08.09.2015</dd>
				</dl>
				<dl class="detail_dl">
					<dt>валютування:</dt>
					<dd>09.09.2015</dd>
				</dl>
				<dl class="detail_dl">
					<dt>оплата:</dt>
					<dd>11.09.2015</dd>
				</dl>
   			</div>

   			<div class="detail_col3">
   				<div class="detail_block">
   					<div class="detail_block_label">Сума</div>
   					<div class="detail_total">129 780 000 . 00  грн</div>
   				</div>
   			</div>
   		</div>

   		<div class="detail_row clearFix">
   			<div class="detail_col2">
   				<div class="detail_block">
   					<div class="detail_block_label">Платник</div>
   					<div class="detail_block_title">Олександрівська клінічна лікарня м. Києва</div>

					<dl class="detail_dl">
						<dt>код ЄДРПОУ:</dt>
						<dd>23123123</dd>
					</dl>

					<dl class="detail_dl">
						<dt>Банк:</dt>
						<dd>Національний Банк України</dd>
					</dl>
					
					<dl class="detail_dl">
						<dt>МФО банку: </dt>
						<dd>111111111</dd>
					</dl>
   				</div>
   			</div>

   			<div class="detail_col2">
   				<div class="detail_block">
   					<div class="detail_block_label">одержувач</div>
   					<div class="detail_block_title">ТОВ Фармацевтична Компанія “Дарниця”</div>

					<dl class="detail_dl">
						<dt>код ЄДРПОУ:</dt>
						<dd>23123123</dd>
					</dl>

					<dl class="detail_dl">
						<dt>Банк:</dt>
						<dd>ПАТ Перший Національний Банк</dd>
					</dl>
					
					<dl class="detail_dl">
						<dt>МФО банку: </dt>
						<dd>111111111</dd>
					</dl>
   				</div>
   			</div>
   		</div>
   </div>
</div>