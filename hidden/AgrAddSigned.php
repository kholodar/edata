<div class="mfiModal actPopupWrap zoomAnim">



   <div class="actPopupS">

		<div class="actTitleS">Додаток до Договору № 123 44213 від 12.09.2015</div>



		<div class="wrapperAct">

			<div class="itemsAct">

				<span class="titleLeft">Контрагент</span>

				<span class="descrLeftItem">Пат “Зіронька” код п/п 123123123</span>

			</div>

			<div class="itemsAct">

				<span class="titleLeft">Місцезнаходження</span>

				<span class="descrLeftItem">Україна, Київська обл., м. Біла Церква, Центральний район, вул. Старовокзальна 23а, корпус 1, квартира/офіс 245</span>

			</div>

			<div class="otherWrapperActSigned minMargTOp">

				<div class="itemsAct">

					<span class="titleLeft">Додаток №</span>

					<span class="descrLeftItem">№1234234 від 09.09.1025</span>

				</div>
				<div class="itemsAct">

					<span class="titleLeft">Предмет додатку</span>

					<span class="descrLeftItem widthObject">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde vitae laboriosam pariatur earum, iusto dignissimos consequuntur, obcaecati cupiditate, quam aspernatur sint magnam vero quae illum,.</span>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Керівник</span>

					<span class="descrLeftItem">Іванов Іван Іванович</span>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Строк дії договору</span>

					<span class="descrLeftItem">09.09.1025 по 09.09.2016, строк у місяцях 12</span>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Вартість договору</span>

					<div class="descrLeftItem">

						<span>123 432 .00 грн</span>

						<span>Обсяг платежів за договором у звітному періоді 123 234 .00 грн</span>

					</div>

				</div>

				<div class="itemsAct">

					<div class="titleLeft">

						<span>Ціна за одиницю</span>

						<span>(за наявності)</span>

					</div>

					<div class="descrLeftItem">

						<span>12 .00</span>

						<span>Кількість закупленного товару, робіт та/або послуг: 12342 шт</span>

					</div>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Процедура закупівлі</span>

					<span class="descrLeftItem">Так</span>

				</div>

			</div>

		</div>

		<div class="actTitleS secTitle">Будь-ласка підпишіть документ вашим електронним сертифікатом</div>

		<div class="signPopupS">
			<div class="wForm wFormFilter wFormDef" data-form="true">
				<div class="bigRow">
		            <div class="wFormRow row_top_line">
					<label for="edrp" class="signLabel wLab">Логін (код ЄДРПОУ)</label>
	                <input type="tel" id="edrpPl" name="sender" placeholder="введіть код" class="wInput" required data-rule-number="true" data-rule-minlength="8">
	            </div>
				</div>
				<div class="bigRow">
		            <div class="wFormRow row_top_line">
						<label for="edrpTa" class="signLabel wLab">Пароль</label>
		                <input type="tel" id="edrpTa" name="recipient" placeholder="введіть пароль" class="wInput" required data-rule-number="true" data-rule-minlength="8">
		            </div>
				</div>
				<div class="bigRow">
		            <div class="wFormRow row_top_line">
		                <div class="wFormInput">   
		                	<label class="wLabel signLabel wLab" for="file-1">Сертифікат</label>
		                    <input type="file" class="wFile" id="sert" name="sertif" required data-rule-filetype="png|gif" data-msg-required="Виберіть файл!">
		                    <label class="wInput wFileVal widthW" for="sert" data-txt='["виберіть файл"]' for="sert">виберіть файл</label>
						</div>
		                <button class="pick_file"><span></span>Вибрати</button> 
		            </div>
				</div>
	            <div class="wFormRow row_submit flr">
	            	<a href="#" class="backListAgr fll"><span></span>редагувати додаток</a>
	                <button type="submit" class="wSubmit button"><span>Підписати</span></button>
	            </div>
	            <div class="clear"></div>
	        </div>
		</div>



   </div>



</div>