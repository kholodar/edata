<div class="mfiModal signPopupWrap zoomAnim">

   <div class="signPopupS">

   		<div class="signTitleS">Ви впевнені, що хочете видалити цей документ?</div>

        <div class="wForm wFormFilter wFormDef" data-form="true">

            <div class="wFormRow row_top_line">

				<p>Видалення документа є незворотньою дією, ви не матимете змогу відновити документ після.</p>

            </div>

            <div class="wFormRow row_submit flr">

                <button class="button closePopup"><span>Видалити</span></button>

            </div>

            <div class="clear"></div>

        </div>

    </div>

</div>