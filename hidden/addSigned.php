<div class="mfiModal actPopupWrap zoomAnim">



   <div class="actPopupS">

		<div class="actTitleS">Додаток до Договору № 123 44213 від 12.09.2015</div>



		<div class="wrapperAct">

			<div class="itemsAct">

				<span class="titleLeft">Контрагент</span>

				<span class="descrLeftItem dopColorF">Пат “Зіронька” код п/п 123123123</span>

			</div>

			<div class="itemsAct">

				<span class="titleLeft">Місцезнаходження</span>

				<span class="descrLeftItem dopColorF">Україна, Київська обл., м. Біла Церква, Центральний район, вул. Старовокзальна 23а, корпус 1, квартира/офіс 245</span>

			</div>

			<div class="otherWrapperActSigned minMargTOp">

				<div class="itemsAct">

					<span class="titleLeft">Додаток №</span>

					<span class="descrLeftItem dopColorF">№1234234 від 09.09.1025</span>

				</div>
				<div class="itemsAct">

					<span class="titleLeft">Предмет додатку</span>

					<span class="descrLeftItem dopColorF widthObject">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde vitae laboriosam pariatur earum, iusto dignissimos consequuntur, obcaecati cupiditate, quam aspernatur sint magnam vero quae illum,.</span>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Керівник</span>

					<span class="descrLeftItem dopColorF">Іванов Іван Іванович</span>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Строк дії договору</span>

					<span class="descrLeftItem dopColorF">09.09.1025 по 09.09.2016, строк у місяцях 12</span>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Вартість договору</span>

					<div class="descrLeftItem dopColorF">

						<span>123 432 .00 грн</span>

						<span>Обсяг платежів за договором у звітному періоді 123 234 .00 грн</span>

					</div>

				</div>

				<div class="itemsAct">

					<div class="titleLeft">

						<span>Ціна за одиницю</span>

						<span>(за наявності)</span>

					</div>

					<div class="descrLeftItem dopColorF">

						<span>12 .00</span>

						<span>Кількість закупленного товару, робіт та/або послуг: 12342 шт</span>

					</div>

				</div>

				<div class="itemsAct">

					<span class="titleLeft">Процедура закупівлі</span>

					<span class="descrLeftItem dopColorF">Так</span>

				</div>

			</div>

		</div>



   </div>



</div>