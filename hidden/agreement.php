﻿<div class="mfiModal zoomAnim">
   <div class="report_popup">
        <div class="agreement_popup_title">Договір № 123-2315523 від 12.09.2015</div>

        <div class="profile_body_nav">
            <a href="" class="table_nav_download">Скачати</a>
        </div>

        <div class="agreement_popup_note">Додано та підписано 12.10.2015</div>

        <div class="agreement_details_options">
            <dl>
                <dt>Контрагент</dt>
                <dd>Пат “Зіронька” код п/п 123123123</dd>
            </dl>

            <dl>
                <dt>Предмет додатку</dt>
                <dd>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error.</dd>
            </dl>

            <dl>
                <dt>Місцезнаходження</dt>
                <dd>Україна, Київська обл., м. Біла Церква, Центральний район, вул. Старовокзальна 23а, корпус 1, квартира/офіс 245</dd>
            </dl>

            <dl>
                <dt>Керівник</dt>
                <dd>Іваненко Іван Іванович</dd>
            </dl>

            <dl>
                <dt>Строк дії договору</dt>
                <dd>з 12.09.2015 по 12.09.2017, строк у місяцях — 12 місяців</dd>
            </dl>

            <dl>
                <dt>Вартість договору</dt>
                <dd>140 000 .00 грн. Обсяг платежів за договором у звітному періоді: 140 000 .00</dd>
            </dl>

            <dl>
                <dt>Процедура закупівлі</dt>
                <dd>Так</dd>
            </dl>

        </div>

        <div class="wTab_Nav">
            <span class="wTab_link" data-tab-container="wTab_Exmpl" data-tab-link="tab_exmA">Додатки (4)</span>
            <span class="wTab_link" data-tab-container="wTab_Exmpl" data-tab-link="tab_exmB">Акти (5)</span>
        </div>

        <div class="wTab_Cantainer wTab_Exmpl">
            <div class="wTab_Block tab_exmA">
                <div class="content">
                    <div class="additional_title">Додаток №123-32 від 09.09.2015</div>
                    
                    <div class="additional_options">
                        <dl>
                            <dt>Предмет додатку</dt>
                            <dd>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error.</dd>
                        </dl>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Керівник</dt>
                                <dd>Іванов Іван Іванович</dd>
                            </dl>

                            <dl>
                                <dt>Строк дії договору</dt>
                                <dd>09.09.1025 по 09.09.2016, строк у місяцях 12</dd>
                            </dl>

                            <dl>
                                <dt>Вартість договору</dt>
                                <dd>123 432 .00 грн <br>Обсяг платежів за договором у звітному періоді 123 234 .00 грн</dd>
                            </dl>
                        </div>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Ціна за одиницю</dt>
                                <dd>12 .00 <br>Кількість закупленного товару, робіт та/або послуг: 12342 шт</dd>
                            </dl>                       

                            <dl>
                                <dt>Процедура закупівлі</dt>
                                <dd>Так</dd>
                            </dl>
                        </div>

                    </div>


                    <div class="additional_title">Додаток №123-32 від 09.09.2015</div>
                    
                    <div class="additional_options">
                        <dl>
                            <dt>Предмет додатку</dt>
                            <dd>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error.</dd>
                        </dl>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Керівник</dt>
                                <dd>Іванов Іван Іванович</dd>
                            </dl>

                            <dl>
                                <dt>Строк дії договору</dt>
                                <dd>09.09.1025 по 09.09.2016, строк у місяцях 12</dd>
                            </dl>

                            <dl>
                                <dt>Вартість договору</dt>
                                <dd>123 432 .00 грн <br>Обсяг платежів за договором у звітному періоді 123 234 .00 грн</dd>
                            </dl>
                        </div>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Ціна за одиницю</dt>
                                <dd>12 .00 <br>Кількість закупленного товару, робіт та/або послуг: 12342 шт</dd>
                            </dl>                       

                            <dl>
                                <dt>Процедура закупівлі</dt>
                                <dd>Так</dd>
                            </dl>
                        </div>

                    </div>


                    <div class="additional_title">Додаток №123-32 від 09.09.2015</div>
                    
                    <div class="additional_options">
                        <dl>
                            <dt>Предмет додатку</dt>
                            <dd>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error.</dd>
                        </dl>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Керівник</dt>
                                <dd>Іванов Іван Іванович</dd>
                            </dl>

                            <dl>
                                <dt>Строк дії договору</dt>
                                <dd>09.09.1025 по 09.09.2016, строк у місяцях 12</dd>
                            </dl>

                            <dl>
                                <dt>Вартість договору</dt>
                                <dd>123 432 .00 грн <br>Обсяг платежів за договором у звітному періоді 123 234 .00 грн</dd>
                            </dl>
                        </div>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Ціна за одиницю</dt>
                                <dd>12 .00 <br>Кількість закупленного товару, робіт та/або послуг: 12342 шт</dd>
                            </dl>                       

                            <dl>
                                <dt>Процедура закупівлі</dt>
                                <dd>Так</dd>
                            </dl>
                        </div>

                    </div>


                    <div class="additional_title">Додаток №123-32 від 09.09.2015</div>
                    
                    <div class="additional_options">
                        <dl>
                            <dt>Предмет додатку</dt>
                            <dd>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error.</dd>
                        </dl>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Керівник</dt>
                                <dd>Іванов Іван Іванович</dd>
                            </dl>

                            <dl>
                                <dt>Строк дії договору</dt>
                                <dd>09.09.1025 по 09.09.2016, строк у місяцях 12</dd>
                            </dl>

                            <dl>
                                <dt>Вартість договору</dt>
                                <dd>123 432 .00 грн <br>Обсяг платежів за договором у звітному періоді 123 234 .00 грн</dd>
                            </dl>
                        </div>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Ціна за одиницю</dt>
                                <dd>12 .00 <br>Кількість закупленного товару, робіт та/або послуг: 12342 шт</dd>
                            </dl>                       

                            <dl>
                                <dt>Процедура закупівлі</dt>
                                <dd>Так</dd>
                            </dl>
                        </div>

                    </div>


                    <div class="additional_title">Додаток №123-32 від 09.09.2015</div>
                    
                    <div class="additional_options">
                        <dl>
                            <dt>Предмет додатку</dt>
                            <dd>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error.</dd>
                        </dl>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Керівник</dt>
                                <dd>Іванов Іван Іванович</dd>
                            </dl>

                            <dl>
                                <dt>Строк дії договору</dt>
                                <dd>09.09.1025 по 09.09.2016, строк у місяцях 12</dd>
                            </dl>

                            <dl>
                                <dt>Вартість договору</dt>
                                <dd>123 432 .00 грн <br>Обсяг платежів за договором у звітному періоді 123 234 .00 грн</dd>
                            </dl>
                        </div>

                        <div class="additional_options_row">
                            <dl>
                                <dt>Ціна за одиницю</dt>
                                <dd>12 .00 <br>Кількість закупленного товару, робіт та/або послуг: 12342 шт</dd>
                            </dl>                       

                            <dl>
                                <dt>Процедура закупівлі</dt>
                                <dd>Так</dd>
                            </dl>
                        </div>

                    </div>

                </div>
            </div>

            <div class="wTab_Block tab_exmB">
                <div class="content">
                    <div class="act_info">Всього актів на сумму &emsp;<span>235 215 750.00 грн</span></div>
                    <div class="act_title">Акти Виконаних Робіт</div>

                    <table class="act_table">
                        <tr>
                            <th>Номер</th>
                            <th>Від</th>
                            <th>Керівник</th>
                            <th>ВАртість</th>
                            <th>Кількість <br><span>закупленного товару, робіт та/або послуг</span></th>
                            <th>ЦІна за одиницю</th>
                        </tr>

                        <tr>
                            <td>№ 12-12364/45</td>
                            <td>12.09.2015</td>
                            <td>Іванов І.І.</td>
                            <td>123 000 .00</td>
                            <td>12 000</td>
                            <td>12 .00</td>
                        </tr>

                        <tr>
                            <td>№ 12-12364/45</td>
                            <td>12.09.2015</td>
                            <td>Іванов І.І.</td>
                            <td>123 000 .00</td>
                            <td>12 000</td>
                            <td>12 .00</td>
                        </tr>

                        <tr>
                            <td>№ 12-12364/45</td>
                            <td>12.09.2015</td>
                            <td>Іванов І.І.</td>
                            <td>123 000 .00</td>
                            <td>12 000</td>
                            <td>12 .00</td>
                        </tr>

                        <tr>
                            <td>№ 12-12364/45</td>
                            <td>12.09.2015</td>
                            <td>Іванов І.І.</td>
                            <td>123 000 .00</td>
                            <td>12 000</td>
                            <td>12 .00</td>
                        </tr>

                        <tr>
                            <td>№ 12-12364/45</td>
                            <td>12.09.2015</td>
                            <td>Іванов І.І.</td>
                            <td>123 000 .00</td>
                            <td>12 000</td>
                            <td>12 .00</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

   </div>
</div>