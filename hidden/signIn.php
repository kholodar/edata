<div class="mfiModal signPopupWrap zoomAnim">
   <div class="signPopupS">
   		<div class="signTitleS">Будь-ласка, підпишіть документ вашим електронним сертифікатом</div>
        <div class="wForm wFormFilter wFormDef" data-form="true">
            <div class="wFormRow row_top_line">
				<label for="edrp" class="signLabel">Логін (код ЄДРПОУ)</label>
                <input type="tel" id="edrpPl" name="sender" placeholder="введіть код" class="wInput" required data-rule-number="true" data-rule-minlength="8">
            </div>
            <div class="wFormRow row_top_line">
				<label for="edrpTa" class="signLabel">Пароль</label>
                <input type="tel" id="edrpTa" name="recipient" placeholder="введіть пароль" class="wInput" required data-rule-number="true" data-rule-minlength="8">

            </div>
            <div class="wFormRow row_top_line">
                <div class="wFormInput">   
                	<label class="wLabel signLabel" for="file-1">Сертифікат</label>
                    <input type="file" class="wFile" id="sert" name="sertif" required data-rule-filetype="png|gif" data-msg-required="Виберіть файл!">
                    <label class="wInput wFileVal" for="sert" data-txt='["виберіть файл"]' for="sert">виберіть файл</label>
				</div>
                <button class="pick_file"><span></span>Вибрати</button> 
            </div>
            <div class="wFormRow row_submit flr">
                <button type="submit" class="wSubmit button"><span>Підписати</span></button>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>