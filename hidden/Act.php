<div class="mfiModal actPopupWrap zoomAnim">

   <div class="actPopupS">
		<div class="actTitleS">Акт Виконаних Робіт  до Договору № 123 44213 від 12.09.2015</div>

		<div class="wrapperAct">
			<div class="itemsAct">
				<span class="titleLeft">Контрагент</span>
				<span class="descrLeftItem">Пат “Зіронька” код п/п 123123123</span>
			</div>
			<div class="itemsAct">
				<span class="titleLeft">Місцезнаходження</span>
				<span class="descrLeftItem">Україна, Київська обл., м. Біла Церква, Центральний район, вул. Старовокзальна 23а, корпус 1, квартира/офіс 245</span>
			</div>
			<div class="otherWrapperActSigned">
				<div class="wForm wFormFilter wFormDef" data-form="true">
						<div class="bigRow">
							<span class="titleLeft crA">Акт №</span>
							<div class="wFormRow row_top_line">
				                <input type="tel" id="edrpPl" name="sender" placeholder="номер акту" class="wInput" required data-rule-number="true" data-rule-minlength="8">
				            </div>
				            <span>від</span>
				            <div class="wFormRow row_top_line">
				                <input type="text" name="date_from_" placeholder="дата" class="date_input" required >
				            </div>
						</div>
						<div class="bigRow">
							<span class="titleLeft crA">Керівник</span>
							<div class="wFormRow">
								<input type="text" id="edrpTa" name="recipientrer" placeholder="Прізвище" class="wInput" required data-rule-word="true" data-rule-minlength="2">
							</div>
							<div class="wFormRow">
								<input type="text" id="edrpTaqwe" name="recipientwe" placeholder="Ім'я" class="wInput" required data-rule-word="true" data-rule-minlength="2">
							</div>
							<div class="wFormRow">
								<input type="text" id="edrpTaqwez" name="recipientqw" placeholder="По-батькові" class="wInput" required data-rule-word="true">
							</div>
						</div>
						<div class="bigRow">
							<span class="titleLeft crA">Вартість послуг</span>
				            <div class="wFormRow row_top_line">
				                <input type="tel" class="totalI" id="edrpTatr" name="recipientgf" placeholder="сумма" class="wInput" required data-rule-number="true">
				            </div>
			                <span class="dopmargRig">грн</span>
				            <span>Обсяг платежів за договором у звітному періоді</span>
							<div class="wFormRow row_top_line">
								<input type="tel" class="totalI" id="edrpTaqwere" name="recipientfgh" placeholder="сумма" class="wInput" required data-rule-number="true">
								<span>грн</span>
							</div>
						</div>
						<div class="bigRow">
				            <div class="wFormRow row_top_line">
								<span class="titleLeft crA">
									<span>Ціна за одиницю</span>
									<span>(за наявності)</span>
								</span>
								<div class="wFormRow row_top_line">
									<input type="tel" class="totalI" id="edrpTaq" name="recipient" placeholder="ціна" class="wInput" required data-rule-number="true">
								</div>
								<span class="dopmargRig">грн</span>
								<span>Кількість закупленного товару, робіт та/або послуг</span>
								<div class="wFormRow row_top_line">
									<input type="tel" class="totalI" id="edrpTasdr" name="recipientr" placeholder="кількість" class="wInput" required data-rule-number="true">
								</div>
				                
				            </div>
						</div>

		            <div class="wFormRow row_submit flr">
		            	<button type="submit" class="wSubmit button_edit"><span>Зберегти зміни</span></button>
		                <button type="submit" class="wSubmit button"><span>Підписати</span></button>
		            </div>
		            <div class="clear"></div>
				</div>
			</div>
		</div>

   </div>

</div>