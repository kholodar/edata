<div class="mfiModal actPopupWrap zoomAnim">

   <div class="actPopupS">
		<div class="actTitleS">Додаток до Договору № 123 44213 від 12.09.2015</div>

		<div class="wrapperAct">
			<div class="itemsAct">
				<span class="titleLeft">Контрагент</span>
				<span class="descrLeftItem">Пат “Зіронька” код п/п 123123123</span>
			</div>
			<div class="itemsAct">
				<span class="titleLeft">Місцезнаходження</span>
				<span class="descrLeftItem">Україна, Київська обл., м. Біла Церква, Центральний район, вул. Старовокзальна 23а, корпус 1, квартира/офіс 245</span>
			</div>
			<div class="otherWrapperActSigned">
					<div class="wForm wFormFilter wFormDef" data-form="true">
						<div class="bigRow">
				            <span class="titleLeft crA">Додаток №</span>
				            <div class="wFormRow row_top_line">
				                <input type="tel" id="edrpPl" name="sender" placeholder="номер акту" class="wInput" required data-rule-number="true" data-rule-minlength="8">
				            </div>
			            	<span>від</span>
			            	<div class="wFormRow row_top_line">
				                <input type="text" name="date_from" placeholder="дата" class="date_input" required >
				            </div>
						</div>
                        <div class="bigRow">
                            <span class="titleLeft crA">Предмет договору</span>
                            <div class="wFormRow ">
                                <textarea class="areaBox" name="" id="" placeholder="предмет договору, не більше 4000 символів"></textarea>
                            </div>
                        </div>
						<div class="bigRow">
							<span class="titleLeft crA">Керівник</span>
							<div class="wFormRow">
								<input type="text" id="edrpTa" name="recipientrer" placeholder="Прізвище" class="wInput" required data-rule-word="true" data-rule-minlength="2">
							</div>
							<div class="wFormRow">
								<input type="text" id="edrpTaqwe" name="recipientwe" placeholder="Ім'я" class="wInput" required data-rule-word="true" data-rule-minlength="2">
							</div>
							<div class="wFormRow">
								<input type="text" id="edrpTaqwe" name="recipientqw" placeholder="По-батькові" class="wInput" required data-rule-word="true">
							</div>
						</div>
						<div class="bigRow">
							<span class="titleLeft crA">Строк дії договору</span>
							<span>з</span>
							<div class="wFormRow">
	                            <div class="wFormInput">
	                                <input type="text" name="date_from_" placeholder="дата" class="date_input widthMore" required >
	                            </div>
	                        </div>
	                        <span>по</span>
	                        <div class="wFormRow">
	                            <div class="wFormInput">
	                                <input type="text" name="date_to" placeholder="дата" class="date_input widthMore" required >
	                            </div>
	                        </div>
	                        <span>строк у місяцях</span>
				            <div class="wFormRow row_top_line">
				                <input type="tel" id="edrpTatr" name="recipientgf" placeholder="число" class="wInput widthNumb" required data-rule-number="true">
				            </div>
						</div>
						<div class="bigRow">
							<span class="titleLeft crA">Вартість договору</span>
				            <div class="wFormRow row_top_line">
				                <input type="tel" class="totalI" id="edrpTatr" name="recipientgfh" placeholder="сумма" class="wInput" required data-rule-number="true">
				            </div>
			                <span  class="dopmargRig">грн</span>
				            <span>Обсяг платежів за договором у звітному періоді</span>
							<div class="wFormRow row_top_line">
								<input type="tel" class="totalI" id="edrpTaqwere" name="recipientfgh" placeholder="сумма" class="wInput" required data-rule-number="true">
								<span>грн</span>
							</div>
						</div>
						<div class="bigRow">
				            <div class="wFormRow row_top_line">
								<span class="titleLeft crA">
									<span>Ціна за одиницю</span>
									<span>(за наявності)</span>
								</span>
								<div class="wFormRow row_top_line">
									<input type="tel" class="totalI" id="edrpTaa" name="recipient" placeholder="ціна" class="wInput" required data-rule-number="true">
								</div>
								<span class="dopmargRig">грн</span>
								<span>Кількість закупленного товару, робіт та/або послуг</span>
								<div class="wFormRow row_top_line">
									<input type="tel" class="totalI" id="edrpTa" name="recipienat" placeholder="кількість" class="wInput" required data-rule-number="true">
								</div>
				                
				            </div>
						</div>
						<div class="bigRow">
				            <span class="titleLeft crA">Процедура закупівлі</span>
		                    <div class="wFormRow">
		                        <label class="wRadio">
		                            <input type="radio" name="radio" required> 
		                            <span>Так</span>
		                        </label>
		                        <label class="wRadio">
		                            <input type="radio" name="radio"> 
		                            <span>Ні</span>
		                        </label>
		                    </div>
						</div>
						<div class="bigRow">
				            <span class="titleLeft crA">Обгрунтування</span>
		                    <div class="wFormRow row_top_line">
		                    	<input type="text" class="WidMax" id="edrpTaad" name="recipienqwet" placeholder="обгрунтування відсутності процедури закупівлі с посиланням на закон" class="wInput" required>
		                    </div>
						</div>
		            <div class="wFormRow row_submit flr">
		            	<button type="submit" class="wSubmit button_edit"><span>Зберегти зміни</span></button>
		                <button type="submit" class="wSubmit button"><span>Створити кабінет</span></button>
		            </div>
		            <div class="clear"></div>
				</div>
			</div>
		</div>

   </div>

</div>