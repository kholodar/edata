$(document).ready(function() {



    // detect transit support

    var transitFlag = Modernizr.cssanimations;



    localStorage.clear(); //не забыть удалить



    function validation() {

        $('.wForm').each(function() {

            var formValid = $(this);

            formValid.validate({

                showErrors: function(errorMap, errorList) {

                    if (errorList.length) {

                        var s = errorList.shift();

                        var n = [];

                        n.push(s);

                        this.errorList = n;

                    }

                    this.defaultShowErrors();

                },

                invalidHandler: function(form, validator) {

                    $(validator.errorList[0].element).trigger('focus');

                    formValid.addClass('no_valid');

                },

                submitHandler: function(form) {

                    formValid.removeClass('no_valid');

                    if (form.tagName === 'FORM') {

                        form.submit();

                    } else {

                        $.ajax({

                            type: 'POST',

                            url: $(form).attr('data-form-url'),
                            /* path/to/file.php */

                            data: $(form).find('select, textarea, input').serializeArray(),

                            dataType: 'json',

                            success: function(data) {



                            },

                            error: function() {

                                $.magnificPopup.open({

                                    items: {

                                        src: '<div class="mfiModal" style="max-width: 300px;"><p>Форма отправлена</p><p>Для верстальщика все ок :)</p></div>',

                                        type: 'inline'

                                    },

                                    removalDelay: 450,

                                    mainClass: 'zoom-in'

                                });

                            }

                        });

                    }

                }

            });

        });



        $('.wForm').on('change', '.wFile', function(event) {

            var m = $(this).prop('multiple'),
                f = this.files,

                label = $(this).siblings('.wFileVal'),
                t = label.data('txt');

            if (f.length) {

                if (m) {

                    var v = t[1].replace('%num%', f.length),
                        a = [];

                    for (var i = 0; i < f.length; i++) {

                        a.push(f[i].name);

                    }

                    label.html('<span>' + v + ' <ins>(' + a.join(', ') + ')</ins></span>');
                    $(this).blur();

                } else {

                    label.html(t[1] + ': ' + f[0].name);
                    $(this).blur();

                }

            } else {

                label.html(t[0]);

            }

        })



        /* Без тега FORM */

        $('.wForm').on('click', '.wSubmit', function(event) {

            var form = $(this).closest('.wForm');

            form.valid();

            if (form.valid()) {

                form.submit();

            }

        });



        /* Сброс Без тега FORM */

        $('.wForm').on('click', '.wReset', function(event) {

            var form = $(this).closest('.wForm');

            if (form.is('DIV')) {

                form.validReset();

            }

        });

    }



    validation();



    /* magnificPopup */



    $('body').magnificPopup({

        delegate: '.mfiA',

        callbacks: {

            elementParse: function(item) {

                mfiID = item.el.data('param').id;

                this.st.ajax.settings = {

                    url: item.el.data('url'),

                    type: 'POST',

                    data: item.el.data('param')

                };

            },

            ajaxContentAdded: function(el) {

                validation();



                if ($('.date_input').length) {

                    $('.date_input').pickadate({

                        format: 'dd.mm.yyyy',

                        today: '',

                        clear: '',

                        close: ''

                    });

                }

            }

        },

        type: 'ajax',

        removalDelay: 300,

        mainClass: 'zoom-in'

    });





    /*wTxt iframe*/

    function wTxtIFRAME() {

        var list = $('.wTxt').find('iframe');

        if (list.length) {

            for (var i = 0; i < list.length; i++) {

                var ifr = list[i];

                var filter = /youtu.be|youtube|vimeo/g; // d

                //if (typeof $(ifr).data('wraped') === 'undefined') { // без фильтра

                if (typeof $(ifr).data('wraped') === 'undefined' && !!ifr.src.match(filter)) {

                    var ratio = (+ifr.height / +ifr.width * 100).toFixed(0);

                    $(ifr).data('wraped', true).wrap('<div class="iframeHolder ratio_' + ratio.slice() + '"></div>');

                }

            }

        }

    }



    $(window).load(function() {

        wTxtIFRAME();

    });



    // svg map



    /*

    $('#map .land').on('click', function(event) {

        event.preventDefault();



        this.classList.toggle("active_path");



        var sibl = $(this).siblings();



        sibl.each(function(index, el) {

            this.classList.remove("active_path");

        });

    });

    */



    $('#map .land').on('mouseover', function(event) {

        $(this).css({

            fill: 'url(#pattern_orange)'

        });

    });



    $('#map .land').on('mouseleave', function(event) {

        $(this).css({

            fill: 'url(#pattern_blue)'

        });

    });



    // -----------------------------------------------------------------



    $('.js-fill-100').on('mouseover', function(event) {

        $('#map .fill-100').each(function(index, el) {

            // this.classList.add("active_path");

            $(this).css({

                fill: 'url(#pattern_orange)'

            });

        });

    });



    $('.js-fill-100').on('mouseleave', function(event) {

        $('#map .fill-100').each(function(index, el) {

            // this.classList.remove("active_path");

            $(this).css({

                fill: 'url(#pattern_blue)'

            });

        });

    });





    $('.js-fill-75').on('mouseover', function(event) {

        $('#map .fill-75').each(function(index, el) {

            // this.classList.add("active_path");

            $(this).css({

                fill: 'url(#pattern_orange)'

            });

        });

    });



    $('.js-fill-75').on('mouseleave', function(event) {

        $('#map .fill-75').each(function(index, el) {

            // this.classList.remove("active_path");

            $(this).css({

                fill: 'url(#pattern_blue)'

            });

        });

    });





    $('.js-fill-60').on('mouseover', function(event) {

        $('#map .fill-60').each(function(index, el) {

            // this.classList.add("active_path");

            $(this).css({

                fill: 'url(#pattern_orange)'

            });

        });

    });



    $('.js-fill-60').on('mouseleave', function(event) {

        $('#map .fill-60').each(function(index, el) {

            // this.classList.remove("active_path");

            $(this).css({

                fill: 'url(#pattern_blue)'

            });

        });

    });





    $('.js-fill-45').on('mouseover', function(event) {

        $('#map .fill-45').each(function(index, el) {

            // this.classList.add("active_path");

            $(this).css({

                fill: 'url(#pattern_orange)'

            });

        });

    });



    $('.js-fill-45').on('mouseleave', function(event) {

        $('#map .fill-45').each(function(index, el) {

            // this.classList.remove("active_path");

            $(this).css({

                fill: 'url(#pattern_blue)'

            });

        });

    });





    $('.js-fill-30').on('mouseover', function(event) {

        $('#map .fill-30').each(function(index, el) {

            // this.classList.add("active_path");

            $(this).css({

                fill: 'url(#pattern_orange)'

            });

        });

    });



    $('.js-fill-30').on('mouseleave', function(event) {

        $('#map .fill-30').each(function(index, el) {

            // this.classList.remove("active_path");

            $(this).css({

                fill: 'url(#pattern_blue)'

            });

        });

    });





    $('.js-fill-15').on('mouseover', function(event) {

        $('#map .fill-15').each(function(index, el) {

            // this.classList.add("active_path");

            $(this).css({

                fill: 'url(#pattern_orange)'

            });

        });

    });



    $('.js-fill-15').on('mouseleave', function(event) {

        $('#map .fill-15').each(function(index, el) {

            // this.classList.remove("active_path");

            $(this).css({

                fill: 'url(#pattern_blue)'

            });

        });

    });



    // date picker



    $('.date_input').pickadate({

        format: 'dd.mm.yyyy',

        today: '',

        clear: '',

        close: ''

    });



    // welcome popup



    function getCookie(name) {

        var matches = document.cookie.match(new RegExp(

            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"

        ));



        return matches ? decodeURIComponent(matches[1]) : undefined;

    }



    var welcomePopup = $('.welcome_popup');



    var wWidth = $(window).width();



    if (wWidth < 1024) {

        var newWidht = wWidth - 20;



        welcomePopup.css({

            width: newWidht,

            marginLeft: -(newWidht / 2)

        });

    }



    $(window).load(function() {

        var hasCookie = getCookie('welcome');



        if (!hasCookie) {

            $('.overlay').addClass('overlay-visible');

            welcomePopup.addClass('welcome_popup-visible');



            document.cookie = "welcome=true";

        }

    });



    $('.button-close').on('click', function(event) {

        event.preventDefault();



        $('.overlay').removeClass('overlay-visible');

        welcomePopup.removeClass('welcome_popup-visible');

    });



    // ----------------------------------------------



    $('#mmenu').clone().append($('.footer_nav li').clone()).mmenu({

        offCanvas: {

            position: "right"

        },

        extensions: ["pageshadow"]

    });



    // ----------------------------------------------



    $('.svg_map path, .svg_map polygon').tipsy({

        gravity: 'sw',

        html: true,

        opacity: 1,

        title: function() {

            var title = $(this).attr('original-title');

            var dataValue = $(this).attr('data-value');



            return '<span class="hint_title">' + title + '</span><br><span class="hint_value">' + dataValue + '</span>';

        }

    });



    // -------------------------------------------



    function setHeight() {

        if ($(window).width() > 720) {

            var wHeight = $(window).height();

            var newHeight = wHeight - (84 + 57);



            $('.search_container').css({

                minHeight: newHeight

            });



            var containerHeight = $('.search_container').height();



            $('.search_container .content').css({

                minHeight: containerHeight

            });

        } else {

            $('.search_container').css({

                minHeight: 0

            });



            $('.search_container .content').css({

                minHeight: 0

            });

        }

    }



    setHeight();



    $(window).resize(function(event) {

        setHeight();

    });



    // ------------------------------------------



    $('.filter_list_link').on('click', function(event) {

        event.preventDefault();



        if ($(this).next().length) {

            $(this).toggleClass('filter_list_link-active').next().slideToggle(200, function() {

                setHeight();

            });

        }

    });



    // ----------------------------------------



    $('select.filter_select, .custom_select').styler();



    // $('.custom_file').styler();



    // ----------------------------------------



    $('.disposers_row').on('click', function(event) {

        event.preventDefault();



        if ($(this).next().length) {

            $(this).parent().toggleClass('disposers_item-active');

        }

    });



    $('.disposers_2lvl_row').on('click', function(event) {

        event.preventDefault();



        if ($(this).next().length) {

            $(this).parent().toggleClass('disposers_2lvl_item-active');

        }

    });



    // ------------------------------------------------



    function setTextpageHeight() {

        if ($(window).width() > 720) {

            var wHeight = $(window).height();

            var newHeight = wHeight - (84 + 89);





            $('.textpage .wConteiner').css({

                minHeight: newHeight

            });



            var contHeight = $('.textpage .wConteiner').height();



            $('.textpage .sideLeft').css({

                minHeight: contHeight

            });



            $('.textpage .sideRight').css({

                minHeight: contHeight

            });





        } else {



            $('.textpage .wConteiner').css({

                minHeight: 0

            });

        }

    }



    $(window).load(function() {

        setTextpageHeight();

    });





    $(window).resize(function(event) {

        setTextpageHeight();

    });



    // disposers filter



    $('.disposers_nav_select select').styler({

        onSelectClosed: function() {



        }

    });



    $('.disposers_year_less').on('click', function(event) {

        var curYear = $(this).next().text();



        $(this).next().text(+curYear - 1);

    });



    $('.disposers_year_more').on('click', function(event) {

        var curYear = $(this).prev().text();



        $(this).prev().text(+curYear + 1);

    });



    var quarters = ["I", "II", "III", "IV"];



    $('.disposers_quarter_less').on('click', function(event) {

        var curQuarterIndex = $(this).next().attr('data-value');



        if (curQuarterIndex > 0) {

            curQuarterIndex = +curQuarterIndex - 1;



            $(this).next().attr('data-value', curQuarterIndex);

        }



        $(this).next().text(quarters[curQuarterIndex]);

    });



    $('.disposers_quarter_more').on('click', function(event) {

        var curQuarterIndex = $(this).prev().attr('data-value');



        if (curQuarterIndex < 3) {

            curQuarterIndex = +curQuarterIndex + 1;



            $(this).prev().attr('data-value', curQuarterIndex);

        }



        $(this).prev().text(quarters[curQuarterIndex]);

    });



    var month = ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"];



    $('.disposers_month_less').on('click', function(event) {

        var curQuarterIndex = $(this).next().attr('data-value');



        if (curQuarterIndex > 0) {

            curQuarterIndex = +curQuarterIndex - 1;



            $(this).next().attr('data-value', curQuarterIndex);

        }



        $(this).next().text(month[curQuarterIndex]);

    });



    $('.disposers_month_more').on('click', function(event) {

        var curQuarterIndex = $(this).prev().attr('data-value');



        if (curQuarterIndex < 11) {

            curQuarterIndex = +curQuarterIndex + 1;



            $(this).prev().attr('data-value', curQuarterIndex);

        }



        $(this).prev().text(month[curQuarterIndex]);

    });



    $('select[name="disposers_select"').on('change', function(event) {

        var selectVal = $(this).val();



        switch (+selectVal) {

            case 1:

                $('.disposers_nav_year').removeClass('invisible');

                $('.disposers_nav_quarter, .disposers_nav_month, .disposers_nav_day').addClass('invisible');

                break;

            case 2:

                $('.disposers_nav_year, .disposers_nav_quarter').removeClass('invisible');

                $('.disposers_nav_month, .disposers_nav_day').addClass('invisible');

                break;

            case 3:

                $('.disposers_nav_year, .disposers_nav_month').removeClass('invisible');

                $('.disposers_nav_quarter, .disposers_nav_day').addClass('invisible');

                break;

            case 4:

                $('.disposers_nav_day').removeClass('invisible');

                $('.disposers_nav_quarter, .disposers_nav_month, .disposers_nav_year').addClass('invisible');

                break;

            default:

                console.log('something wrong');

        }

    });



    $('body').on('click', '.pick_file-select', function(event) {

        event.preventDefault();



        $('.wFile').trigger('click');

    });



    $('body').on('click', '.pick_file-error', function(event) {
        event.preventDefault();
        $('.wFileVal').text('');
    });

    $('body').on('click', '.closePopup', function(event) {
        event.preventDefault();

        $.magnificPopup.close();
    });

    function wTab(t) {  
        t.parent().children('.curr').removeClass('curr');
        t.addClass('curr');
        $('.' + t.attr('data-tab-container')).children('.curr').removeClass('curr');
        $('.' + t.attr('data-tab-container')).children('.' + t.attr('data-tab-link')).addClass('curr');
    }

    $('body').on('click', '.wTab_Nav .wTab_link', function(event) {
        if ($(this).hasClass('curr')) {
            return false;
        } else {
            wTab($(this));
        }
    });

});
